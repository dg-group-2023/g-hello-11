terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
  backend "http" {
  }
}

provider "aws" {
  region = var.region
}

resource "aws_s3_bucket" "daniel-tf-bucket" {
  //bucket = var.bucket
  tags = {
    //Name        = var.bucket
    Environment = "Dev"
  }
}

resource "aws_s3_bucket_public_access_block" "bucket-public-access-block" {
  bucket = aws_s3_bucket.daniel-tf-bucket.bucket
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

resource "aws_s3_object" "daniel-object" {
  bucket = aws_s3_bucket.daniel-tf-bucket.bucket
  key    = "g-hello-11-0.0.1-SNAPSHOT.jar"
  source = "build/libs/g-hello-11-0.0.1-SNAPSHOT.jar"
}

resource "aws_vpc" "daniel-vpc" {
  cidr_block = var.vpc_cidr_block
  enable_dns_support = true
  enable_dns_hostnames = true
  tags = {
    Name = "daniel-vpc"
  }
}

resource "aws_subnet" "daniel-public-subnet" {
  vpc_id     = aws_vpc.daniel-vpc.id
  cidr_block = var.public_subnet_cidr_block
}

resource "aws_subnet" "daniel-private-subnet" {
  vpc_id     = aws_vpc.daniel-vpc.id
  cidr_block = var.private_subnet_cidr_block
}

resource "aws_internet_gateway" "daniel-gateway" {
  vpc_id = aws_vpc.daniel-vpc.id

  tags = {
    Name = "daniel-vpc-ig"
  }
}

resource "aws_route_table" "daniel_rt" {
  vpc_id = aws_vpc.daniel-vpc.id

  route {
    cidr_block = var.aws_route_table_cidr_block
    gateway_id = aws_internet_gateway.daniel-gateway.id
  }

  tags = {
    Name = "daniel-rt"
  }
}

resource "aws_route_table_association" "public_subnet" {
  count = 1
  subnet_id = element(aws_subnet.daniel-public-subnet[*].id, count.index)
  route_table_id = aws_route_table.daniel_rt.id
}

resource "aws_security_group" "daniel-tf-security-group" {
  name        = "daniel-tf-security-group"
  description = "Allow SSH & TCP inbound traffic"
  vpc_id      = aws_vpc.daniel-vpc.id

  ingress {
    description      = "SSH from Anywhere"
    from_port        = var.port22
    to_port          = var.port22
    protocol         = var.protocol
    cidr_blocks      = [var.cidr_blocks]
    //ipv6_cidr_blocks = [var.ipv6_cidr_blocks]
  }

  ingress {
    description      = "Spring Boot port from Anywhere"
    from_port        = var.port8080
    to_port          = var.port8080
    protocol         = var.protocol
    cidr_blocks      = [var.cidr_blocks]
    //ipv6_cidr_blocks = [var.ipv6_cidr_blocks]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = [var.cidr_blocks]
    ipv6_cidr_blocks = [var.ipv6_cidr_blocks]
  }

  tags = {
    Name = "daniel-tf-security-group"
  }

}

resource "aws_iam_role_policy" "daniel-tf-policy" {
  name = "daniel-tf-policy"
  role = aws_iam_role.daniel-tf-role.id

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  policy = jsonencode({
    "Version": "2012-10-17",
    "Statement": [
      {
        "Effect": "Allow",
        "Action": [
          "s3:ListBucket",
          "s3:GetObject"
        ],
        "Resource": [
          "${aws_s3_bucket.daniel-tf-bucket.arn}",
          "${aws_s3_bucket.daniel-tf-bucket.arn}/*",
        ]
      },
      {
        "Effect": "Allow",
        "Action": "s3:ListAllMyBuckets",
        "Resource": "*"
      }
    ]
  })
}

resource "aws_iam_role" "daniel-tf-role" {
  name = "daniel-tf-role"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })
}

resource "aws_iam_instance_profile" "daniel-tf-profile" {
  name = "daniel-tf-profile"
  role = aws_iam_role.daniel-tf-role.name
}

resource "aws_instance" "web" {
  ami           = var.ami
  instance_type = var.instance_type
  vpc_security_group_ids = [
    aws_security_group.daniel-tf-security-group.id
  ]
  iam_instance_profile = aws_iam_instance_profile.daniel-tf-profile.name
  subnet_id = aws_subnet.daniel-public-subnet.id
  associate_public_ip_address = true
  key_name = var.key_name
  user_data = <<EOF
    #!/bin/bash
    yum update -y
    yum install -y java-17-amazon-corretto-headless
    aws s3api get-object --bucket ${aws_s3_bucket.daniel-tf-bucket.bucket} --key ${aws_s3_object.daniel-object.key} ${aws_s3_object.daniel-object.key}
    java -jar ${aws_s3_object.daniel-object.key}
  EOF
  //user_data = "${file("startup.sh")}"
  tags = {
    Name = "daniel-tf-web-server"
  }
}