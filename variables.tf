variable "region" {
    description = "us-east-1"
    type = string
    default = "us-east-1"
}

variable "bucket" {
     description = "daniel-tf-bucket"
     type = string
     default = "daniel-tf-bucket"
}

variable "ami" {
    description = "ami"
    type = string
    default = "ami-005f9685cb30f234b"
}

variable "instance_type" {
    description = "t2.micro"
    type = string
    default = "t2.micro"
}

variable "key_name" {
    description = "daniel-key-pair"
    type = string
    default = "daniel-key-pair"
}

variable "cidr_blocks" {
    description = "0.0.0.0/0"
    type = string
    default = "0.0.0.0/0"
}

variable "ipv6_cidr_blocks" {
    description = "::/0"
    type = string
    default = "::/0"
}

variable "protocol" {
    description = "tcp"
    type = string
    default = "tcp"
}

variable "port22" {
    description = "22"
    type = string
    default = "22"
}

variable "port8080" {
    description = "8080"
    type = string
    default = "8080"
}

variable "vpc_cidr_block" {
    description = "10.0.0.0/16"
    type = string
    default = "10.0.0.0/16"
}

variable "public_subnet_cidr_block" {
    description = "10.0.1.0/24"
    type = string
    default = "10.0.1.0/24"
}

variable "private_subnet_cidr_block" {
    description = "10.0.4.0/24"
    type = string
    default = "10.0.4.0/24"
}

variable "aws_route_table_cidr_block" {
    description = "0.0.0.0/0"
    type = string
    default = "0.0.0.0/0"
}